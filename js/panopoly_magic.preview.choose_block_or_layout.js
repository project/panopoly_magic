(function ($, Drupal) {

  Drupal.behaviors.panopolyMagicChooseBlock = {
    attach(context) {
      if ($(context).is('.panopoly-magic-choose-block')) {
        // If a block preview is shown, then mark the link for it as active.
        $(once('panopoly-magic-choose-block-link', '.js-panopoly-magic-choose-block-link', context)).click(function () {
          $('.panopoly-magic-choose-block-link-active', context)
            .removeClass('panopoly-magic-choose-block-link-active');
          $('.panopoly-magic-choose-block-link-wrapper-active', context)
            .removeClass('panopoly-magic-choose-block-link-wrapper-active');

          $(this)
            .addClass('panopoly-magic-choose-block-link-active')
            .closest('.panopoly-magic-choose-block-link-wrapper')
            .addClass('panopoly-magic-choose-block-link-wrapper-active');
        });

        const $block_links = $('.panopoly-magic-choose-block-link-wrapper', context);
        if ($block_links.length > 0) {
          // If we loaded a category in either in Single or Disabled mode, then
          // we focus on the first block link.
          $(':focusable:first', $block_links.get(0)).focus();
        }
        else {
          const $add_links = $('.js-layout-builder-block-link', context);
          if ($add_links.length > 0) {
            // If we loaded a category in either Manual or Automatic mode, then
            // we focus on the first add link.
            $add_links.get(0).focus();
          }
          else {
            // We can't do this right away, because the content isn't in the
            // dialog yet - that happens after a moment.
            setTimeout(function () {
              // By default, focus on the dialog close button (if we can find one).
              const $close_btn = $(context)
                .closest('.ui-dialog')
                .find('.ui-dialog-titlebar-close');
              if ($close_btn.length > 0) {
                $close_btn.focus();
              }
              else {
                // Otherwise, focus on the first category.
                $('.panopoly-magic-choose-block-categories :focusable:first', context).focus();
              }
            }, 0);
          }
        }
      }
      else if ($(context).is('.panopoly-magic-preview-wrapper') && $(context).parents('.panopoly-magic-choose-block').length > 0) {
        // If we loaded a preview, focus the first focusable element in it.
        $(':focusable:first', context).focus();
      }
    }
  };

  Drupal.behaviors.panopolyMagicChooseLayout = {
    attach(context) {
      $(once('panopoly-magic-choose-layout', '.js-panopoly-magic-choose-layout-category-link', context)).click(function () {
        const selector = $(this).attr('href');
        if (selector.startsWith('#')) {
          // Update the category button with the current category class.
          $('.panopoly-magic-choose-layout-current-category')
            .removeClass('panopoly-magic-choose-layout-current-category');
          $(this.parentElement)
            .addClass('panopoly-magic-choose-layout-current-category');

          // Hide all the other lists, and show this one.
          $('.js-panopoly-magic-choose-layout-category-list').addClass('hidden');
          $(selector)
            .removeClass('hidden')
            .find(':focusable:first')
            .focus();

          return false;
        }
      });
    }
  };

})(jQuery, Drupal);
