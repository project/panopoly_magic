<?php

namespace Drupal\panopoly_magic\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\layout_builder\Form\AddBlockForm;
use Drupal\layout_builder\SectionStorageInterface;

/**
 * Enhances the add block form with live preview.
 */
class LayoutBuilderAddBlockForm extends AddBlockForm {

  use LayoutBuilderBlockFormTrait;

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, SectionStorageInterface $section_storage = NULL, $delta = NULL, $region = NULL, $plugin_id = NULL) {
    $form = parent::buildForm($form, $form_state, $section_storage, $delta, $region, $plugin_id);
    $this->alterFormForPreview($form, $form_state);

    $block_definition = $this->getBlockPluginDefinition();
    $form['#title'] = $this->t('Add @block', [
      '@block' => $block_definition['admin_label'],
    ]);

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $this->suppressValidationForPreview($form, $form_state);
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Handle preview mode.
    if ($form_state->getValue('op') == $form['actions']['preview']['#value']) {
      return $this->rebuildPreview($form, $form_state);
    }

    parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function successfulAjaxSubmit(array $form, FormStateInterface $form_state) {
    // When in preview mode return the rebuilt layout.
    if ($form_state->getValue('op') == $form['actions']['preview']['#value']) {
      return $this->rebuildPreview($form, $form_state);
    }

    return parent::successfulAjaxSubmit($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function submitLabel() {
    return $this->t('Save');
  }

}
