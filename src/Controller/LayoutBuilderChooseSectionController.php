<?php

namespace Drupal\panopoly_magic\Controller;

use Drupal\Component\Utility\Html;
use Drupal\Core\Ajax\AjaxHelperTrait;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Layout\LayoutInterface;
use Drupal\Core\Layout\LayoutPluginManagerInterface;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\layout_builder\Context\LayoutBuilderContextTrait;
use Drupal\layout_builder\LayoutBuilderHighlightTrait;
use Drupal\layout_builder\SectionStorageInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Defines a controller to choose a new section.
 *
 * @internal
 *   Controller classes are internal.
 */
class LayoutBuilderChooseSectionController implements ContainerInjectionInterface {

  use AjaxHelperTrait;
  use LayoutBuilderContextTrait;
  use LayoutBuilderHighlightTrait;
  use StringTranslationTrait;

  /**
   * The layout manager.
   *
   * @var \Drupal\Core\Layout\LayoutPluginManagerInterface
   */
  protected $layoutManager;

  /**
   * ChooseSectionController constructor.
   *
   * @param \Drupal\Core\Layout\LayoutPluginManagerInterface $layout_manager
   *   The layout manager.
   */
  public function __construct(LayoutPluginManagerInterface $layout_manager) {
    $this->layoutManager = $layout_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.core.layout')
    );
  }

  /**
   * Choose a layout plugin to add as a section.
   *
   * @param \Drupal\layout_builder\SectionStorageInterface $section_storage
   *   The section storage.
   * @param int $delta
   *   The delta of the section to splice.
   * @param array $definitions
   *   The layout definitions.
   *
   * @return array
   *   The render array.
   */
  protected function buildLayoutList(SectionStorageInterface $section_storage, int $delta, array $definitions) {
    $items = [];
    foreach ($definitions as $plugin_id => $definition) {
      $layout = $this->layoutManager->createInstance($plugin_id);
      $item = [
        '#type' => 'link',
        '#title' => [
          'icon' => $definition->getIcon(60, 80, 1, 3),
          'label' => [
            '#type' => 'container',
            '#children' => $definition->getLabel(),
          ],
        ],
        '#url' => $this->getLayoutUrl($section_storage, $delta, $layout),
        '#attributes' => $this->getAjaxAttributes(),
      ];
      $items[$plugin_id] = $item;
    }

    $list = [
      '#theme' => 'item_list__layouts',
      '#items' => $items,
      '#attributes' => [
        'class' => [
          'layout-selection',
        ],
        'data-layout-builder-target-highlight-id' => $this->sectionAddHighlightId($delta),
      ],
    ];

    return [
      '#type' => 'container',
      '#attributes' => [
        'class' => [
          'js-panopoly-magic-choose-layout-category-list',
        ],
      ],
      'list' => $list,
    ];
  }

  /**
   * Choose a layout plugin to add as a section.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   * @param \Drupal\layout_builder\SectionStorageInterface $section_storage
   *   The section storage.
   * @param int $delta
   *   The delta of the section to splice.
   *
   * @return array
   *   The render array.
   */
  public function build(Request $request, SectionStorageInterface $section_storage, int $delta) {
    $definitions = $this->layoutManager->getFilteredDefinitions('layout_builder', [], ['section_storage' => $section_storage]);
    $grouped_definitions = $this->layoutManager->getGroupedDefinitions($definitions);

    $build = [
      '#type' => 'container',
      '#attributes' => [
        'class' => [
          'panopoly-magic-choose-layout',
        ],
      ],
    ];
    $build['categories'] = [
      '#theme' => 'item_list',
      '#list_type' => 'ul',
      '#attributes' => [
        'class' => ['panopoly-magic-choose-layout-categories'],
      ],
      '#items' => [],
    ];
    $build['layouts'] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => ['panopoly-magic-choose-layout-list'],
      ],
    ];
    $build['#attached']['library'][] = 'panopoly_magic/preview.choose_block_or_layout';
    $build['#cache'] = [
      'contexts' => [
        'languages',
        'theme',
        'url.path',
        'url.query_args',
        'user.permissions',
      ],
    ];

    $first = TRUE;
    foreach (array_keys($grouped_definitions) as $category) {
      $id = 'panopoly-magic-layout-category-' . Html::cleanCssIdentifier($category);

      $category_element = [
        '#type' => 'link',
        '#url' => Url::fromUri('internal:#' . $id),
        '#title' => $category,
        '#wrapper_attributes' => [
          'class' => [
            'panopoly-magic-choose-layout-category',
          ],
        ],
        '#attributes' => [
          'class' => [
            'js-panopoly-magic-choose-layout-category-link',
          ],
        ],
      ];
      $build['layouts'][$category] = $this->buildLayoutList($section_storage, $delta, $grouped_definitions[$category]);
      $build['layouts'][$category]['#attributes']['id'] = $id;

      if ($first) {
        $first = FALSE;
        $category_element['#wrapper_attributes']['class'][] = 'panopoly-magic-choose-layout-current-category';
      }
      else {
        $build['layouts'][$category]['#attributes']['class'][] = 'hidden';
      }

      $build['categories']['#items'][] = $category_element;
    }

    if (count($grouped_definitions) === 1) {
      $build['#attributes']['class'][] = 'panopoly-magic-choose-layout-single';
      unset($build['categories']);
    }
    else {
      $build['#attributes']['class'][] = 'panopoly-magic-choose-layout-multiple';
    }

    return $build;
  }

  /**
   * Gets the URL for the layout link.
   *
   * @param \Drupal\layout_builder\Annotation\SectionStorageInterface $section_storage
   *   The section storage.
   * @param int $delta
   *   The section delta.
   * @param \Drupal\Core\Layout\LayoutInterface $layout
   *   The layout.
   *
   * @return \Drupal\Core\Url
   *   The URL.
   */
  protected function getLayoutUrl(SectionStorageInterface $section_storage, $delta, LayoutInterface $layout) {
    return Url::fromRoute(
      $layout instanceof PluginFormInterface ? 'layout_builder.configure_section' : 'layout_builder.add_section',
      [
        'section_storage_type' => $section_storage->getStorageType(),
        'section_storage' => $section_storage->getStorageId(),
        'delta' => $delta,
        'plugin_id' => $layout->getPluginId(),
      ]
    );
  }

  /**
   * Get dialog attributes if an ajax request.
   *
   * @return array
   *   The attributes array.
   */
  protected function getAjaxAttributes() {
    if ($this->isAjax()) {
      return [
        'class' => ['use-ajax'],
        'data-dialog-type' => 'dialog',
        'data-dialog-renderer' => 'off_canvas',
      ];
    }
    return [];
  }

}
